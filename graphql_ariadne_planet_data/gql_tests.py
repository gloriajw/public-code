# -*- coding: iso8859-15 -*-
import os,sys

appdir = os.path.abspath(os.path.dirname(__file__))
projdir = os.path.abspath(os.path.join(appdir,'..'))
if projdir not in sys.path:
    sys.path.append(appdir)
    sys.path.append(projdir)

import arrow
import json
from loguru import logger
import requests
import unittest

global headers
global access_key
global refresh_key
headers = {'Content-Type': 'application/json'}
access_key = None
refresh_key = None

class GQLTests(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.moondata='https://api.le-systeme-solaire.net/rest.php/bodies?rowData=false&filter%5B%5D=isMoon%2Ceq%2Ctrue&satisfy=any'
        self.planetdata='https://api.le-systeme-solaire.net/rest.php/bodies?rowData=false&filter%5B%5D=isPlanet%2Ceq%2Ctrue&satisfy=any'
        self.BASE_URL='http://127.0.0.1:8001/'

    @classmethod
    def tearDownClass(self):
        pass

    def test_0_inject_data(self):
        url = f'{self.BASE_URL}graphql/'

        #import pdb;pdb.set_trace()
        moondata = requests.get(self.moondata)
        moondata = json.loads(moondata.text)
        for i in moondata['bodies']:
            if i['aroundPlanet']:
                i['aroundPlanet'] = i['aroundPlanet']['planet']
            del i['isPlanet']
            del i['rel']
            del i['moons']
            del i['id']
            data={"query":"mutation save_moon($moon:MoonInput){save_moon(moon: $moon) {error}}","variables":{"moon":i,"operationName":"save_moon"}}
            # get works also, but data is exposed in URL, so we don't use it
            # unless absolutely necessary (like in verify user/password links
            # behind the firewall).
            #r = requests.get(url+'query=' + json.dumps(data), headers=headers)
            r = requests.post(url, data=json.dumps(data), headers=headers)
            print(r.text)
            result = json.loads(r.text)
            print(result['data']['save_moon']['error'])

        planetdata = requests.get(self.planetdata)
        planetdata = json.loads(planetdata.text)
        for i in planetdata['bodies']:
            del i['aroundPlanet']
            del i['isPlanet']
            del i['rel']
            del i['moons']
            del i['id']
            data={"query":"mutation save_planet($planet:PlanetInput){save_planet(planet: $planet) {error}}","variables":{"planet":i,"operationName":"save_planet"}}
            # get works also, but data is exposed in URL, so we don't use it
            # unless absolutely necessary (like in verify user/password links
            # behind the firewall).
            #r = requests.get(url+'query=' + json.dumps(data), headers=headers)
            r = requests.post(url, data=json.dumps(data), headers=headers)
            print(r.text)
            result = json.loads(r.text)
            print(result['data']['save_planet']['error'])

    def test_1_get_planets(self):
        url = f'{self.BASE_URL}graphql/'
        data={"query":"{find_planets (planet_name:\"jupiter\") {name englishName semimajorAxis perihelion aphelion eccentricity inclination mass {massValue} vol {volValue} density gravity escape meanRadius equaRadius polarRadius flattening dimension sideralOrbit sideralRotation discoveredBy discoveryDate alternativeName axialTilt avgTemp mainAnomaly argPeriapsis longAscNode bodyType moons {name englishName semimajorAxis perihelion aphelion eccentricity inclination} }}"}
        r = requests.post(url, data=json.dumps(data), headers=headers)
        #print(r.text)
        result = json.loads(r.text)
        print(result)
    
        data={"query":"{find_planets (planet_name:null) {name englishName semimajorAxis perihelion aphelion eccentricity inclination mass {massValue} vol {volValue} density gravity escape meanRadius equaRadius polarRadius flattening dimension sideralOrbit sideralRotation discoveredBy discoveryDate alternativeName axialTilt avgTemp mainAnomaly argPeriapsis longAscNode bodyType moons {name englishName semimajorAxis perihelion aphelion eccentricity inclination} }}"}
        r = requests.post(url, data=json.dumps(data), headers=headers)
        #print(r.text)
        result = json.loads(r.text)
        print(result)
    
if __name__ == '__main__':
    unittest.main()
