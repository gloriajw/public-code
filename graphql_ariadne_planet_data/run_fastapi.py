# -*- coding: iso8859-15 -*-
import os,sys

appdir = os.path.abspath(os.path.dirname(__file__))
projdir = os.path.abspath(os.path.join(appdir,'..'))
if projdir not in sys.path:
    sys.path.append(appdir)
    sys.path.append(projdir)

from ariadne import (gql, ObjectType,QueryType,
    MutationType,make_executable_schema)
from ariadne.asgi import GraphQL
from dataclasses import dataclass,field
from fastapi import FastAPI,status,Body,Header,Request
from fastapi.exceptions import RequestValidationError
from loguru import logger
from starlette.exceptions import HTTPException
from starlette.responses import RedirectResponse
from typing import List,Annotated,Union,Optional
import traceback

from planet_moon_data import PlanetMoonData

#from run.fastapi_exception_handlers import RequestExceptionHandling

ENV='Development'
FASTAPI_HOST='127.0.0.1'
FASTAPI_PORT=8001

def find_in_gql_header(info,variable):
    '''
    nginx should limit HTTP headers to some reasonable max (1k for example).
    '''
    b_variable = str.encode(variable)
    if info and 'request' in info.context and 'headers' in info.context['request'].scope:
        for header in info.context['request'].scope['headers']:
            if header[0] == b_variable:
               return header[1].decode('utf-8') 

app = FastAPI()
#REH = RequestExceptionHandling(logger)

#app.add_exception_handler(RequestValidationError, REH.request_validation_exception_handler)
#app.add_exception_handler(HTTPException, REH.http_exception_handler)
#app.add_exception_handler(Exception, REH.unhandled_exception_handler)

DEBUG=False
if ENV != 'Production':
    logger.warning("WARNING: ENV is NOT set to Production. Is this what you want?")
    from fastapi.middleware.cors import CORSMiddleware

    origins = [
        f"http://localhost:{FASTAPI_PORT}",
        f"http://{FASTAPI_HOST}:{FASTAPI_PORT}",
        ]
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
        )
    DEBUG=True
else:
    logger.warning("WARNING: SECURE_MODE is set to TRUE. Is this what you want? In MCC or Production the answer is YES.")
    from fastapi.middleware.cors import CORSMiddleware

    origins = [
        "*.haydiagi.io",
        ]
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
        )
    DEBUG=False

# Create GraphQL type definitions
type_defs = gql("""
    scalar BigInt

    input MassInput {
        massValue: Float
        massExponent: BigInt
    }

    type Mass {
        massValue: Float
        massExponent: BigInt
    }

    input VolumeInput {
        volValue: Float
        volExponent: BigInt
    }

    type Volume {
        volValue: Float
        volExponent: BigInt
    }

    input MoonInput {
        id: String
        name: String
        englishName: String
        semimajorAxis: BigInt
        perihelion: BigInt
        aphelion: BigInt
        eccentricity: Float
        inclination: Float
        mass: MassInput
        vol: VolumeInput
        density: Float
        gravity: Float
        escape: Float
        meanRadius: Float
        equaRadius: Float
        polarRadius: Float
        flattening: Float
        dimension: String
        sideralOrbit: Float
        sideralRotation: Float
        aroundPlanet: String
        discoveredBy: String
        discoveryDate: String
        alternativeName: String
        axialTilt: Float
        avgTemp: BigInt
        mainAnomaly: Float
        argPeriapsis: Float
        longAscNode: Float
        bodyType: String
    }

    type Moon {
        id: String
        name: String
        englishName: String
        semimajorAxis: BigInt
        perihelion: BigInt
        aphelion: BigInt
        eccentricity: Float
        inclination: Float
        mass: Mass
        vol: Volume
        density: Float
        gravity: Float
        escape: Float
        meanRadius: Float
        equaRadius: Float
        polarRadius: Float
        flattening: Float
        dimension: String
        sideralOrbit: Float
        sideralRotation: Float
        aroundPlanet: String
        discoveredBy: String
        discoveryDate: String
        alternativeName: String
        axialTilt: Float
        avgTemp: BigInt
        mainAnomaly: Float
        argPeriapsis: Float
        longAscNode: Float
        bodyType: String
    }

    input PlanetInput {
        id: String
        name: String
        englishName: String
        moons: [MoonInput]
        semimajorAxis: BigInt
        perihelion: BigInt
        aphelion: BigInt
        eccentricity: Float
        inclination: Float
        mass: MassInput
        vol: VolumeInput
        density: Float
        gravity: Float
        escape: Float
        meanRadius: Float
        equaRadius: Float
        polarRadius: Float
        flattening: Float
        dimension: String
        sideralOrbit: Float
        sideralRotation: Float
        aroundPlanet: String
        discoveredBy: String
        discoveryDate: String
        alternativeName: String
        axialTilt: Float
        avgTemp: BigInt
        mainAnomaly: Float
        argPeriapsis: Float
        longAscNode: Float
        bodyType: String
    }

    type Planet {
        id: String
        name: String
        englishName: String
        moons: [Moon]
        semimajorAxis: BigInt
        perihelion: BigInt
        aphelion: BigInt
        eccentricity: Float
        inclination: Float
        mass: Mass
        vol: Volume
        density: Float
        gravity: Float
        escape: Float
        meanRadius: Float
        equaRadius: Float
        polarRadius: Float
        flattening: Float
        dimension: String
        sideralOrbit: Float
        sideralRotation: Float
        aroundPlanet: Float
        discoveredBy: String
        discoveryDate: String
        alternativeName: String
        axialTilt: Float
        avgTemp: BigInt
        mainAnomaly: Float
        argPeriapsis: Float
        longAscNode: Float
        bodyType: String
    }

    type BoolResponse {
        success: Boolean
    }

    type ErrorResponse {
        error: String
    }

    type Query {
        find_planets(planet_name:String): [Planet]
        find_moons(moon_name:String): [Moon]
    }

    type Mutation {
        save_moon(moon:MoonInput): ErrorResponse
        save_planet(planet:PlanetInput): ErrorResponse
    }

""")

@dataclass
class Mass:
    massValue: float
    massExponent: int

@dataclass
class Volume:
    volValue: float
    volExponent: int

@dataclass
class Moon:
    name: str
    englishName: str
    semimajorAxis: int
    perihelion: int
    aphelion: int
    eccentricity: float
    inclination: float
    mass: Mass
    vol: Volume
    density: float
    gravity: float
    escape: float
    meanRadius: float
    equaRadius: float
    polarRadius: float
    flattening: float
    dimension: str
    sideralOrbit: float
    sideralRotation: float
    aroundPlanet: str
    discoveredBy: str
    discoveryDate: str
    alternativeName: str
    axialTilt: float
    avgTemp: int
    mainAnomaly: float
    argPeriapsis: float
    longAscNode: float
    bodyType: str

@dataclass
class Planet:
    name: str
    englishName: str
    semimajorAxis: int
    perihelion: int
    aphelion: int
    eccentricity: float
    inclination: float
    mass: Mass
    vol: Volume
    density: float
    gravity: float
    escape: float
    meanRadius: float
    equaRadius: float
    polarRadius: float
    flattening: float
    dimension: str
    sideralOrbit: float
    sideralRotation: float
    discoveredBy: str
    discoveryDate: str
    alternativeName: str
    axialTilt: float
    avgTemp: int
    mainAnomaly: float
    argPeriapsis: float
    longAscNode: float
    bodyType: str
    moons: list[Moon] = None

@dataclass
class BoolResponse:
    response: bool = None

@dataclass
class ErrorResponse:
    error: str = None

query = QueryType()
mutation = MutationType()

@query.field("find_planets")
def resolve_find_planets(_,info,planet_name=None):
    pmd = PlanetMoonData(logger)
    planets = pmd._find_planet(planet_name)
    moons = {}
    outbound = []
    for p in planets:
        del p['_id']
        p['moons'] = [Moon(**m) for m in pmd._find_moon(planet_name=p['name'])]
        outbound.append(Planet(**p))
    return outbound

@query.field("find_moons")
def resolve_find_moons(_,info,moon_name=None):
    moons = []
    pmd = PlanetMoonData(logger)
    for m in pmd._find_moon(moon_name):
        del m['_id']
        moons.append(Moon(**m))
    return moons

@mutation.field("save_moon")
def resolve_save_moon(_, info, moon:dict):
    try:
        pmd = PlanetMoonData(logger)
        pmd._save_moon(**moon)
        return BoolResponse(True)
    except:
        return ErrorResponse(traceback.format_exc())

@mutation.field("save_planet")
def resolve_save_planet(_, info, planet:dict):
    try:
        pmd = PlanetMoonData(logger)
        pmd._save_planet(**planet)
        return BoolResponse(True)
    except:
        return ErrorResponse(traceback.format_exc())

'''
GQL schema parsing and go-live.
'''
schema = make_executable_schema(type_defs, query, mutation)

'''
Attach GraphQL endpoint to FastAPI app
'''
app.mount("/graphql/", GraphQL(schema,debug=DEBUG))

'''
FastAPI ReSTful endpoints: call underlying graphql methods, return same data.
'''
# This link is embedded in emails for first-time user email verification.
@app.get("/get_planets/")
async def rest_get_planets(planet_name: str):
    return resolve_get_planets(None,None,planet_name)

# Run the FastAPI app
if __name__ == "__main__":
    '''
    import uvicorn
    import uvicorn.config

    @app.on_event('startup')
    async def local_startup_event(): 
        # loguru Logging Options
        logger.remove()
        logger.add(
            sys.stderr,
            colorize=True,
            format="<green>{time}</green> <level>{message}</level>",
        )
        logger.add("/var/log/gunicorn/access.log", rotation="500 MB", enqueue=True, backtrace=True, diagnose=True)
        logger.add("/var/log/gunicorn/error.log", rotation="500 MB", enqueue=True, backtrace=True, diagnose=True)
        logger.add("/var/log/gunicorn/log.json", rotation="500 MB", enqueue=True, backtrace=True, diagnose=True, serialize=True)
        logger.level('DEBUG')
    '''


    '''
    Production run should be invoked by gunicorn or some other ASGI spawner.
    uvicorn.run(app, host=FASTAPI_HOST, port=FASTAPI_PORT)
    '''

    print('''
    When testing a single instance directly, run:
    uvicorn run_fastapi:app --reload --host 0.0.0.0 --port 8001 &
    ''')
