#!/usr/bin/bash

running_container=`docker ps | grep gql_planet_data | awk '{print $1}'`
if [[ -n "${running_container}" ]]
then
    docker stop ${running_container} && docker container rm -f ${running_container}
fi

docker run -d --name gql_planet_data --log-driver syslog \
    -p 8001:8001 -e MONGODB_HOST=127.0.0.1 -e MONGO_PORT=27018 \
    -e fastapi_port=8001 \
    gql_planet_data:latest
    #--mount type=bind,source="/home/user1/",target="/home/app_user/" \
    #--add-host meetup-test:10.116.0.2 \

echo "Running: `docker ps|grep gql_planet_data`"


