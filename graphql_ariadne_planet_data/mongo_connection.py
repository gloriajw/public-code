# -*- coding: iso8859-15 -*-
import os,sys
import pymongo
from pymongo import ReturnDocument
import time

appdir = os.path.abspath(os.path.dirname(__file__))
projdir = os.path.abspath(os.path.join(appdir,'..'))
if projdir not in sys.path:
    sys.path.append(appdir)
    sys.path.append(projdir)

def connect_to_mongo(db_name,connected_conn=None,connected_db=None,connect_string="mongodb://{MONGODB_HOST}:{MONGODB_PORT}/"):
    '''
    This allows for a retry of the connection to mongod, which may periodically fail due to
    the binary and data mountpoint not being ready at that moment.

    It is also used to test for staleness in an existing connection, which may happen if connections
    are not opened correctly in threads. This should auto-correct.

    The DB does not have to exist when we connect to it. Mongo will create it, and
    no error will occur. So please use constants for DB names, to avoid misspellings.
    '''
    mongo_connection = None
    mongo_db = None
    for i in range(3):
        try:
            if connected_db != None:
                for j in range(1):
                    try:
                        '''
                        If existing connection is passed in, test for staleness.
                        Retry this only once.
                        '''
                        connected_db.list_collection_names()
                        #print("Mongo connection is good...")
                        return connected_conn,connected_db
                    except:
                        print("Mongo connection is stale, refreshing...")
                        sys.stdout.flush()
                        connected_conn = None
                        connected_db = None
                        break

            '''
            Seems counter-intuitive to not immediately connect. But for threading this
            makes sense: mongo will lazy-load and connect upon first use, and you won't get that
            warning of reuse inside of the thread.
            '''
            mongo_connection = pymongo.MongoClient(connect_string,connect=False)
            mongo_db = mongo_connection[db_name]
            #mongo_db.list_collection_names()
            break
        except:
            print("Mongo Connection failed, sleeping then retrying")
            time.sleep(3)

    return mongo_connection,mongo_db
