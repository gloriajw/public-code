# -*- coding: iso8859-15 -*-
import os,sys

appdir = os.path.abspath(os.path.dirname(__file__))
projdir = os.path.abspath(os.path.join(appdir,'..'))
if projdir not in sys.path:
    sys.path.append(appdir)
    sys.path.append(projdir)

import arrow
import pymongo
import uuid

from mongo_connection import connect_to_mongo

MONGODB_NAME='PLANET_MOON_DATA'
MONGODB_CONNECT_STRING='mongodb://127.0.0.1:27018/'

global mongo_connection
global mongo_db

mongo_connection,mongo_db = connect_to_mongo(
    db_name=MONGODB_NAME,
    connected_conn=None,
    connected_db=None,
    connect_string=MONGODB_CONNECT_STRING)

moon_collection = mongo_db['MOON_COLLECTION']
planet_collection = mongo_db['PLANET_COLLECTION']

for dbindex in moon_collection.list_indexes():
    if dbindex['name'] != '_id_':
        try:
            moon_collection.drop_index(dbindex['name'])
        except:
            pass

for dbindex in planet_collection.list_indexes():
    if dbindex['name'] != '_id_':
        try:
            planet_collection.drop_index(dbindex['name'])
        except:
            pass

#moon_collection.create_index([('englishName',pymongo.TEXT)],unique=True)
moon_collection.create_index([('englishName',pymongo.TEXT)])
moon_collection.create_index(['aroundPlanet'])
#planet_collection.create_index([('englishName',pymongo.TEXT)],unique=True)
planet_collection.create_index([('englishName',pymongo.TEXT)])

class PlanetMoonData:
    def __init__(self,logger):
        self.logger = logger

    def _save_planet(self,**planet):
        global mongo_connection
        global mongo_db

        mongo_connection,mongo_db = connect_to_mongo(
            db_name=MONGODB_NAME,
            connected_conn=mongo_connection,
            connected_db=mongo_db,
            connect_string=MONGODB_CONNECT_STRING)

        result = planet_collection.replace_one(
            {'englishName':planet['englishName']}, planet, upsert=True)
        print(result.matched_count,result.modified_count,result.upserted_id)

    def _save_moon(self,**moon):
        global mongo_connection
        global mongo_db

        mongo_connection,mongo_db = connect_to_mongo(
            db_name=MONGODB_NAME,
            connected_conn=mongo_connection,
            connected_db=mongo_db,
            connect_string=MONGODB_CONNECT_STRING)

        result = moon_collection.replace_one(
            {'englishName':moon['englishName']}, moon, upsert=True)
        print(result.matched_count,result.modified_count,result.upserted_id)

    def _find_planet(self,planet_name=None):
        global mongo_connection
        global mongo_db

        mongo_connection,mongo_db = connect_to_mongo(
            db_name=MONGODB_NAME,
            connected_conn=mongo_connection,
            connected_db=mongo_db,
            connect_string=MONGODB_CONNECT_STRING)

        if not planet_name:
            return list(planet_collection.find())
        return list(planet_collection.find(
            {'$text':{'$search':planet_name,'$caseSensitive':False}}))

    def _find_moon(self,planet_name=None,moon_name=None):
        global mongo_connection
        global mongo_db

        mongo_connection,mongo_db = connect_to_mongo(
            db_name=MONGODB_NAME,
            connected_conn=mongo_connection,
            connected_db=mongo_db,
            connect_string=MONGODB_CONNECT_STRING)

        if planet_name:
            return list(moon_collection.find({'aroundPlanet':planet_name}))
        elif moon_name:
            return list(moon_collection.find({'$text':{'$search':moon_name,'$caseSensitive':False}}))

        return list(moon_collection.find())

if __name__ == '__main__':
    import pdb;pdb.set_trace()
    from loguru import logger
    pmd = PlanetMoonData(logger)
    print(pmd._find_planet('mars mercury venus'))
    print(pmd._find_planet('m'))
